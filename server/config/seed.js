/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var _thing = require('../api/thing/thing.model');

var _thing2 = _interopRequireDefault(_thing);

var _user = require('../api/user/user.model');

var _user2 = _interopRequireDefault(_user);

var _tradehistory = require('../api/tradehistory/tradehistory.model');

var _tradehistory2 = _interopRequireDefault(_tradehistory);

var _mymessage = require('../api/mymessage/mymessage.model');

var _mymessage2 = _interopRequireDefault(_mymessage);

var _myaccount = require('../api/myaccount/myaccount.model');

var _myaccount2 = _interopRequireDefault(_myaccount);

var _cupon = require('../api/cupon/cupon.model');

var _cupon2 = _interopRequireDefault(_cupon);

var _feelist = require('../api/feelist/feelist.model');

var _feelist2 = _interopRequireDefault(_feelist);

var _trucks = require('../api/trucks/trucks.model');

var _trucks2 = _interopRequireDefault(_trucks);

var _translist = require('../api/translist/translist.model');

var _translist2 = _interopRequireDefault(_translist);

var _power = require('../api/power/power.model');

var _power2 = _interopRequireDefault(_power);

var _customers = require('../api/customers/customers.model');

var _customers2 = _interopRequireDefault(_customers);

var _info = require('../api/info/info.model');

var _info2 = _interopRequireDefault(_info);

var _insure = require('../api/insure/insure.model');

var _insure2 = _interopRequireDefault(_insure);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_info2.default.find({}).remove().then(function () {
  _info2.default.create({ ordernum: '1', type: 'bbs', info: '平台新版上线', adddate: '', url: '', active: true }, { ordernum: '2', type: 'bbs', info: '平台更新公告', adddate: '', url: '', active: true }, { ordernum: '3', type: 'bbs', info: '平台发放新补贴', adddate: '', url: '', active: true }, { ordernum: '4', type: 'bbs', info: '平台新功能增加', adddate: '', url: '', active: true }, { ordernum: '5', type: 'bbs', info: '平台举办线下活动', adddate: '', url: '', active: true }, { ordernum: '1', type: 'process', info: '开户流程', adddate: '', url: '', active: true }, { ordernum: '2', type: 'process', info: '合约', adddate: '', url: '', active: true }, { ordernum: '3', type: 'process', info: '运力购买规则', adddate: '', url: '', active: true }, { ordernum: '4', type: 'process', info: '运力指数说明', adddate: '', url: '', active: true }, { ordernum: '5', type: 'process', info: '注意事项', adddate: '', url: '', active: true }, { ordernum: '1', type: 'index', info: '运价交易指数', adddate: '', url: '', active: true }, { ordernum: '2', type: 'index', info: '运价预估参考指数', adddate: '', url: '', active: true }, { ordernum: '3', type: 'index', info: '油价指数', adddate: '', url: '', active: true }, { ordernum: '4', type: 'index', info: '保险价格指数', adddate: '', url: '', active: true }, { ordernum: '5', type: 'index', info: '运力指数', adddate: '', url: '', active: true }, { ordernum: '1', type: 'company', info: '公司介绍', adddate: '', url: '', active: true }, { ordernum: '2', type: 'company', info: '联系我们', adddate: '', url: '', active: true }, { ordernum: '3', type: 'company', info: '公司经营状况', adddate: '', url: '', active: true }, { ordernum: '4', type: 'company', info: '公司股权说明', adddate: '', url: '', active: true }, { ordernum: '5', type: 'company', info: '合作伙伴', adddate: '', url: '', active: true }, { ordernum: '1', type: 'news', info: '平台开业了', adddate: '', url: '', active: true }, { ordernum: '2', type: 'news', info: '平台引入战略合作伙伴', adddate: '', url: '', active: true }, { ordernum: '3', type: 'news', info: '公司获得A轮融资', adddate: '', url: '', active: true }, { ordernum: '4', type: 'news', info: '大事记-公司领导访谈录', adddate: '', url: '', active: true }, { ordernum: '5', type: 'news', info: '政府认可,我们的骄傲', adddate: '', url: '', active: true }, { ordernum: '1', type: 'cindex', info: '今日平台交易数量', adddate: '', url: '', active: true }, { ordernum: '2', type: 'cindex', info: '今日平台成交总价', adddate: '', url: '', active: true }, { ordernum: '3', type: 'cindex', info: '今日平台承运数量', adddate: '', url: '', active: true }, { ordernum: '4', type: 'cindex', info: '今日平台活跃客户数量', adddate: '', url: '', active: true }, { ordernum: '5', type: 'cindex', info: '今日平台访问量', adddate: '', url: '', active: true });
});

_insure2.default.find({}).remove().then(function () {
  _insure2.default.create({ name: '仅交强险', info: '仅交强险', active: true }, { name: '交强险&定额货运险', info: '交强险&定额货运险', active: true }, { name: '交强险&全额货运险', info: '交强险&全额货运险', active: true });
});

_customers2.default.find({}).remove().then(function () {
  _customers2.default.create({ username: 'cyt', companyname: '苏州清芝电子科技有限公司', companyaddr: '苏州市吴中区东吴北路吴中大厦', contact: '崔云涛', email: 'cuicuiyuntao85@hotmail.com',
    qq: '1097642', weixin: 'cuicuiyuntao85@hotmail.com', website: 'www.蚂蚁运力.com', mobilenumber: '13386020378', phone: '13386020378',
    companypic: '', producttype: '互联网信息公司', info: '测试数据', active: true }, { username: 'zlr', companyname: '苏州清芝电子科技有限公司', companyaddr: '苏州市吴中区东吴北路吴中大厦', contact: '朱莲茹', email: 'cuicuiyuntao85@hotmail.com',
    qq: '1097642', weixin: 'cuicuiyuntao85@hotmail.com', website: 'www.蚂蚁运力.com', mobilenumber: '13386020378', phone: '13386020378',
    companypic: '', producttype: '互联网信息公司', info: '测试数据', active: true });
});

_power2.default.find({}).remove().then(function () {});

_translist2.default.find({}).remove().then(function () {});

_trucks2.default.find({}).remove().then(function () {
  _trucks2.default.create({ username: 'cyt', adddate: new Date(), paizhao: '沪G82859', chexing: '载重卡车', cheling: '1',
    changshang: '东风', baoxian: '仅交强险', zaizhong: '10', chechang: '6', chegao: '3', chekuan: '2.5', rongji: '24',
    chejiahao: '12345678901234567', fadongjihao: '12345678901234567', info: '测试数据', active: true }, { username: 'zlr', adddate: new Date(), paizhao: '苏E82859', chexing: '载重卡车', cheling: '1',
    changshang: '东风', baoxian: '仅交强险', zaizhong: '10', chechang: '6', chegao: '3', chekuan: '2.5', rongji: '24',
    chejiahao: '12345678901234567', fadongjihao: '12345678901234567', info: '测试数据', active: true });
});

_feelist2.default.find({}).remove().then(function () {});

_cupon2.default.find({}).remove().then(function () {
  _cupon2.default.create({ username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '保险优惠卷', cuponprice: '50.00', cuponcontent: '保险满1000送50', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'cyt', cupontype: '交易优惠卷', cuponprice: '5.00', cuponcontent: '交易手续费抵扣', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '10.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true }, { username: 'zlr', cupontype: '运费优惠卷', cuponprice: '15.00', cuponcontent: '运输费抵扣卷', cupondate: new Date(), limitdate: new Date(), active: true });
});

_myaccount2.default.find({}).remove().then(function () {
  _myaccount2.default.create({ username: 'cyt', accountpic: '', accounttype: '运输公司', pcredit: '41', ycredit: '60', account: '0.00', yajin: '50000', active: true }, { username: 'zlr', accountpic: '', accounttype: '运输公司', pcredit: '100', ycredit: '100', account: '10000.00', yajin: '50000', active: true });
});

_tradehistory2.default.find({}).remove().then(function () {});

_mymessage2.default.find({}).remove().then(function () {});

_thing2.default.find({}).remove().then(function () {
  _thing2.default.create({
    name: 'Development Tools',
    info: 'Integration with popular tools such as Bower, Grunt, Babel, Karma, ' + 'Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, ' + 'Stylus, Sass, and Less.'
  }, {
    name: 'Server and Client integration',
    info: 'Built with a powerful and fun stack: MongoDB, Express, ' + 'AngularJS, and Node.'
  }, {
    name: 'Smart Build System',
    info: 'Build system ignores `spec` files, allowing you to keep ' + 'tests alongside code. Automatic injection of scripts and ' + 'styles into your index.html'
  }, {
    name: 'Modular Structure',
    info: 'Best practice client and server structures allow for more ' + 'code reusability and maximum scalability'
  }, {
    name: 'Optimized Build',
    info: 'Build process packs up your templates as a single JavaScript ' + 'payload, minifies your scripts/css/images, and rewrites asset ' + 'names for caching.'
  }, {
    name: 'Deployment Ready',
    info: 'Easily deploy your app to Heroku or Openshift with the heroku ' + 'and openshift subgenerators'
  });
});

_user2.default.find({}).remove().then(function () {
  _user2.default.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@example.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@example.com',
    password: 'admin'
  }, {
    provider: 'local',
    name: '崔云涛',
    email: 'cyt',
    password: 'cyt'
  }, {
    provider: 'local',
    name: '朱莲茹',
    email: 'zlr',
    password: 'zlr'
  }).then(function () {
    console.log('finished populating users');
  });
});
//# sourceMappingURL=seed.js.map
