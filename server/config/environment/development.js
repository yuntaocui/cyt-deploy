'use strict';

// Development specific configuration
// ==================================

module.exports = {

  // MongoDB connection options
  mongo: {
    // uri: 'mongodb://demo.1-dingdan.com/cyt-dev'
    uri: 'mongodb://localhost/cyt-dev'
  },

  // Seed database on startup
  seedDB: true

};
//# sourceMappingURL=development.js.map
