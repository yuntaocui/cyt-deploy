/**
 * Main application routes
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (app) {
  // Insert routes below
  app.use('/api/infos', require('./api/info'));
  app.use('/api/insures', require('./api/insure'));
  app.use('/api/feelists', require('./api/feelist'));
  app.use('/api/cupons', require('./api/cupon'));
  app.use('/api/mymessages', require('./api/mymessage'));
  app.use('/api/customerss', require('./api/customers'));
  app.use('/api/truckss', require('./api/trucks'));
  app.use('/api/myaccounts', require('./api/myaccount'));
  app.use('/api/translists', require('./api/translist'));
  app.use('/api/powers', require('./api/power'));
  app.use('/api/tradehistorys', require('./api/tradehistory'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));
  app.use('/auth', require('./auth').default);

  app.route('/img/*').get(function (req, res) {
    var p = _path2.default.join(_environment2.default.root, 'server', req.path);
    console.log('img path', p);
    res.sendFile(p);
  });

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(_errors2.default[404]);

  // All other routes should redirect to the index.html
  app.route('/*').get(function (req, res) {
    res.sendFile(_path2.default.resolve(app.get('appPath') + '/index.html'));
  });
};

var _errors = require('./components/errors');

var _errors2 = _interopRequireDefault(_errors);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _environment = require('./config/environment');

var _environment2 = _interopRequireDefault(_environment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=routes.js.map
