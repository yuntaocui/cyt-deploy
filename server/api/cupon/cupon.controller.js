/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/cupons              ->  index
 * POST    /api/cupons              ->  create
 * GET     /api/cupons/:id          ->  show
 * PUT     /api/cupons/:id          ->  update
 * DELETE  /api/cupons/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.pageshow = pageshow;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _cupon = require('./cupon.model');

var _cupon2 = _interopRequireDefault(_cupon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function respondWithResultA(res, ret, statusCode) {
  statusCode = statusCode || 200;
  return function () {
    if (ret) {
      res.status(statusCode).json(ret);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Cupons
function index(req, res) {
  return _cupon2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Cupon from the DB
function show(req, res) {
  return _cupon2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

function pageshow(req, res) {
  var _this = this;

  var paginationConf = {
    currentPage: 0,
    totalItems: 0,
    itemsPerPage: 0,
    pagesLength: 0,
    totalPrice: 0,
    data: []
  };
  return _cupon2.default.find({ username: req.body.username }).exec().then(function (ret) {
    paginationConf.currentPage = req.body.currentPage;
    paginationConf.totalItems = ret.length;

    ret.forEach(function (cuponprice) {
      paginationConf.totalPrice += cuponprice.cuponprice;
    }, _this);
    paginationConf.pagesLength = Math.ceil(ret.length / req.body.itemsPerPage);
    paginationConf.itemsPerPage = req.body.itemsPerPage;
    _cupon2.default.find({ username: req.body.username }).limit(req.body.itemsPerPage).skip(req.body.currentPage * req.body.itemsPerPage).exec().then(function (ret) {
      paginationConf.data = ret;
    }).then(respondWithResultA(res, paginationConf)).catch(handleError(res));
  });
}

// Creates a new Cupon in the DB
function create(req, res) {
  return _cupon2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Updates an existing Cupon in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _cupon2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Cupon from the DB
function destroy(req, res) {
  return _cupon2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=cupon.controller.js.map
