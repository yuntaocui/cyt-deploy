/**
 * Translist model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _translist = require('./translist.model');

var _translist2 = _interopRequireDefault(_translist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TranslistEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
TranslistEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _translist2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    TranslistEvents.emit(event + ':' + doc._id, doc);
    TranslistEvents.emit(event, doc);
  };
}

exports.default = TranslistEvents;
//# sourceMappingURL=translist.events.js.map
