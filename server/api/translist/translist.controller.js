/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/translists              ->  index
 * POST    /api/translists              ->  create
 * GET     /api/translists/:id          ->  show
 * PUT     /api/translists/:id          ->  update
 * DELETE  /api/translists/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.show = show;
exports.pageshow = pageshow;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _translist = require('./translist.model');

var _translist2 = _interopRequireDefault(_translist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Translists
function index(req, res) {
  return _translist2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Translist from the DB
function show(req, res) {
  return _translist2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

function respondWithResultA(res, ret, statusCode) {
  statusCode = statusCode || 200;
  return function () {
    if (ret) {
      res.status(statusCode).json(ret);
      console.log(ret);
    }
  };
}

function pageshow(req, res) {
  var paginationConf = {
    currentPage: 0,
    totalItems: 0,
    itemsPerPage: 0,
    pagesLength: 0,
    data: []
  };
  if (req.body.username === '*') {
    return _translist2.default.find().exec().then(function (ret) {
      paginationConf.currentPage = req.body.currentPage;
      paginationConf.totalItems = ret.length;
      paginationConf.pagesLength = Math.ceil(ret.length / req.body.itemsPerPage);
      paginationConf.itemsPerPage = req.body.itemsPerPage;
      _translist2.default.find().limit(req.body.itemsPerPage).skip(req.body.currentPage * req.body.itemsPerPage).exec().then(function (ret) {
        paginationConf.data = ret;
      }).then(respondWithResultA(res, paginationConf)).catch(handleError(res));
    });
  } else {
    return _translist2.default.find({ userholder: req.body.username }).exec().then(function (ret) {
      paginationConf.currentPage = req.body.currentPage;
      paginationConf.totalItems = ret.length;
      paginationConf.pagesLength = Math.ceil(ret.length / req.body.itemsPerPage);
      paginationConf.itemsPerPage = req.body.itemsPerPage;
      _translist2.default.find({ username: req.body.username }).limit(req.body.itemsPerPage).skip(req.body.currentPage * req.body.itemsPerPage).exec().then(function (ret) {
        paginationConf.data = ret;
      }).then(respondWithResultA(res, paginationConf)).catch(handleError(res));
    });
  }
}

// Creates a new Translist in the DB
function create(req, res) {
  return _translist2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Updates an existing Translist in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _translist2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Translist from the DB
function destroy(req, res) {
  return _translist2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=translist.controller.js.map
