'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MymessageSchema = new _mongoose2.default.Schema({
  username: String,
  messagedate: Date,
  messagecontent: String,
  readflag: Boolean,
  active: Boolean
});

exports.default = _mongoose2.default.model('Mymessage', MymessageSchema);
//# sourceMappingURL=mymessage.model.js.map
