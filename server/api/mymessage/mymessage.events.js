/**
 * Mymessage model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _mymessage = require('./mymessage.model');

var _mymessage2 = _interopRequireDefault(_mymessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MymessageEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
MymessageEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _mymessage2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    MymessageEvents.emit(event + ':' + doc._id, doc);
    MymessageEvents.emit(event, doc);
  };
}

exports.default = MymessageEvents;
//# sourceMappingURL=mymessage.events.js.map
