/**
 * Info model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _info = require('./info.model');

var _info2 = _interopRequireDefault(_info);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var InfoEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
InfoEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _info2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    InfoEvents.emit(event + ':' + doc._id, doc);
    InfoEvents.emit(event, doc);
  };
}

exports.default = InfoEvents;
//# sourceMappingURL=info.events.js.map
