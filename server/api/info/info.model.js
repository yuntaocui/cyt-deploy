'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var InfoSchema = new _mongoose2.default.Schema({
  ordernum: Number,
  type: String,
  info: String,
  adddate: Date,
  url: String,
  active: Boolean
});

exports.default = _mongoose2.default.model('Info', InfoSchema);
//# sourceMappingURL=info.model.js.map
