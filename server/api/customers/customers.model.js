'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CustomersSchema = new _mongoose2.default.Schema({
  username: String,
  companyname: String,
  companyaddr: String,
  contact: String,
  email: String,
  qq: String,
  weixin: String,
  website: String,
  mobilenumber: String,
  phone: String,
  companypic: String,
  producttype: String,
  info: String,
  active: Boolean
});

exports.default = _mongoose2.default.model('Customers', CustomersSchema);
//# sourceMappingURL=customers.model.js.map
