/**
 * Customers model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _customers = require('./customers.model');

var _customers2 = _interopRequireDefault(_customers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CustomersEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
CustomersEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _customers2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    CustomersEvents.emit(event + ':' + doc._id, doc);
    CustomersEvents.emit(event, doc);
  };
}

exports.default = CustomersEvents;
//# sourceMappingURL=customers.events.js.map
