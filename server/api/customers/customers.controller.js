/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/customerss              ->  index
 * POST    /api/customerss              ->  create
 * GET     /api/customerss/:id          ->  show
 * PUT     /api/customerss/:id          ->  update
 * DELETE  /api/customerss/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.upload = upload;
exports.index = index;
exports.show = show;
exports.showuser = showuser;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _customers = require('./customers.model');

var _customers2 = _interopRequireDefault(_customers);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _environment = require('../../config/environment');

var _environment2 = _interopRequireDefault(_environment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function upload(req, res, next) {
  var fstream;
  var res1 = res;
  req.pipe(req.busboy);
  req.busboy.on('file', function (fieldname, file, filename) {
    console.log("Uploading: " + filename);

    //Path where image will be uploaded
    fstream = _fs2.default.createWriteStream(_environment2.default.root + '/server/img/' + filename);
    file.pipe(fstream);
    fstream.on('close', function () {
      console.log("Upload Finished of " + filename);
      res.send('back'); //where to go next
      //  res1.status(200).json({filename: filename});
    });
  });

  // fs.readFile(req.files.displayImage.path, function (err, data) {
  //   // ...
  //   var newPath = __dirname + '/img/' + filename;
  //   fs.writeFile(newPath, data, function (err) {
  //     res.redirect("back");
  //   });
  // });
}

// Gets a list of Customerss
function index(req, res) {
  return _customers2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Customers from the DB
function show(req, res) {
  return _customers2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

function showuser(req, res) {
  return _customers2.default.find({ username: req.params.username }).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Customers in the DB
function create(req, res) {
  return _customers2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Updates an existing Customers in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _customers2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Customers from the DB
function destroy(req, res) {
  return _customers2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=customers.controller.js.map
