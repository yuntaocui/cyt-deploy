'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TrucksSchema = new _mongoose2.default.Schema({
  username: String,
  adddate: Date,

  paizhao: String,
  chexing: String,
  cheling: String,
  changshang: String,
  baoxian: String,

  zaizhong: String,
  chechang: String,
  chegao: String,
  chekuan: String,
  rongji: String,
  chejiahao: String,
  fadongjihao: String,

  info: String,
  active: Boolean
});

exports.default = _mongoose2.default.model('Trucks', TrucksSchema);
//# sourceMappingURL=trucks.model.js.map
