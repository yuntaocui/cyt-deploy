/**
 * Tradehistory model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _tradehistory = require('./tradehistory.model');

var _tradehistory2 = _interopRequireDefault(_tradehistory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TradehistoryEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
TradehistoryEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _tradehistory2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    TradehistoryEvents.emit(event + ':' + doc._id, doc);
    TradehistoryEvents.emit(event, doc);
  };
}

exports.default = TradehistoryEvents;
//# sourceMappingURL=tradehistory.events.js.map
