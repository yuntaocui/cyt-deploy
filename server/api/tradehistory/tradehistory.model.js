'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TradehistorySchema = new _mongoose2.default.Schema({
  ticket: String,
  username: String,
  orgholder: String,
  newholder: String,
  adddate: Date,
  startPlace: String,
  endPlace: String,
  orgprice: Number,
  newprice: Number,
  yunshudate: Date,
  yunshutype: String,
  yunshuinfo: String,
  insuretype: String,
  transtype: String,
  truck: String,
  active: Boolean
});

exports.default = _mongoose2.default.model('Tradehistory', TradehistorySchema);
//# sourceMappingURL=tradehistory.model.js.map
