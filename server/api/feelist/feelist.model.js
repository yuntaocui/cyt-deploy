'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FeelistSchema = new _mongoose2.default.Schema({
  username: String,
  feedate: Date,
  feetype: String,
  feecontent: String,
  feeprice: Number,
  feeresult: Boolean,
  feevoice: Boolean,
  feecontract: Boolean,
  active: Boolean
});

exports.default = _mongoose2.default.model('Feelist', FeelistSchema);
//# sourceMappingURL=feelist.model.js.map
