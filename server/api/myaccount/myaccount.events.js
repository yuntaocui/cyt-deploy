/**
 * Myaccount model events
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _events = require('events');

var _myaccount = require('./myaccount.model');

var _myaccount2 = _interopRequireDefault(_myaccount);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MyaccountEvents = new _events.EventEmitter();

// Set max event listeners (0 == unlimited)
MyaccountEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  _myaccount2.default.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function (doc) {
    MyaccountEvents.emit(event + ':' + doc._id, doc);
    MyaccountEvents.emit(event, doc);
  };
}

exports.default = MyaccountEvents;
//# sourceMappingURL=myaccount.events.js.map
