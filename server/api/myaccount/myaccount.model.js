'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MyaccountSchema = new _mongoose2.default.Schema({
  username: String,
  accountpic: String,
  accounttype: String,
  pcredit: Number,
  ycredit: Number,
  account: Number,
  yajin: Number,
  active: Boolean
});

exports.default = _mongoose2.default.model('Myaccount', MyaccountSchema);
//# sourceMappingURL=myaccount.model.js.map
