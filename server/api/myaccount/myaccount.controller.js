/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/myaccounts              ->  index
 * POST    /api/myaccounts              ->  create
 * GET     /api/myaccounts/:id          ->  show
 * PUT     /api/myaccounts/:id          ->  update
 * DELETE  /api/myaccounts/:id          ->  destroy
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.index = index;
exports.showuser = showuser;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _myaccount = require('./myaccount.model');

var _myaccount2 = _interopRequireDefault(_myaccount);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _lodash2.default.merge(entity, updates);
    console.log(updated);
    return updated.save().then(function (updated) {
      return updated;
    });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove().then(function () {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Myaccounts
function index(req, res) {
  return _myaccount2.default.find().exec().then(respondWithResult(res)).catch(handleError(res));
}

function showuser(req, res) {
  return _myaccount2.default.find({ username: req.params.username }).exec().then(respondWithResult(res)).catch(handleError(res));
}

// Gets a single Myaccount from the DB
function show(req, res) {
  return _myaccount2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).catch(handleError(res));
}

// Creates a new Myaccount in the DB
function create(req, res) {
  return _myaccount2.default.create(req.body).then(respondWithResult(res, 201)).catch(handleError(res));
}

// Updates an existing Myaccount in the DB
function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return _myaccount2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(saveUpdates(req.body)).then(respondWithResult(res)).catch(handleError(res));
}

// Deletes a Myaccount from the DB
function destroy(req, res) {
  return _myaccount2.default.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).catch(handleError(res));
}
//# sourceMappingURL=myaccount.controller.js.map
