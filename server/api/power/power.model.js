'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PowerSchema = new _mongoose2.default.Schema({
  username: String,
  adddate: Date,
  startPlace: String,
  endPlace: String,
  price: Number,
  yunshudate: Date,
  yunshutype: String,
  yunshuinfo: String,
  insuretype: String,
  truck: String,
  powerstatus: String,
  active: Boolean
});

exports.default = _mongoose2.default.model('Power', PowerSchema);
//# sourceMappingURL=power.model.js.map
